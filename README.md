# 软件工程课程设计信息管理系统
一、 项目搭建 
把原来以ssh为框架的系统拆分为三个项目，分别为dubbo-client、dubbo-service、dubbo-web。 
1. dubbo-client：消费者，主要实现controller层控制跳转等。 
2. dubbo-service：主要是接口定义，供dubbo-client调用，以及dubbo-web的service层去实现该接口，分离这一层的好处就是前端项目调用接口时候，直接调用dubbo-service的接口即可，不需要关注后端如何实现；而service层来具体实现该接口，进行业务逻辑处理，不需要关注dubbo-client的controller层如何调用。 
3. dubbo-web：提供者，主要是service、dao实现，结合数据层实现后端业务逻辑处理。 

二、项目配置 
1. 将dubbo-service打包成jar加入到dubbo-client和dubbo-web中。

2.在pom.xml中加入dubbo配置：

3.消费者的dubbo-config.xml：

4 . 提供者的dubbo-config.xml：

5 . 配置applicationContext-spring.xml：
   <import resource="classpath:dubbo_config.xml"/>
二、 项目编写 
1. 消费者服务：

2 . 提供者服务：

三、 项目结果 
3.1启动zookeeper 
3.2将项目部署到Tomact 
3.3主要功能界面 


具体博客地址：https://blog.csdn.net/srain13/article/details/80717353