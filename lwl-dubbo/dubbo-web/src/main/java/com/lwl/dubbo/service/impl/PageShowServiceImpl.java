package com.lwl.dubbo.service.impl;


import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lwl.dubbo.dao.impl.PageShowDaoImpl;
import com.lwl.dubbo.domain.PageShow;
import com.lwl.dubbo.service.IPageShowService;



@Service
public class PageShowServiceImpl extends BaseServiceImpl<PageShow> implements
        IPageShowService {
	
	@Resource(name = "pageShowDao")
	protected PageShowDaoImpl pageShowDao;
	
	public void setPageShowDao(PageShowDaoImpl pageShowDao) {
		this.pageShowDao = pageShowDao;
		super.setBaseDao(pageShowDao);
	}

	  @SuppressWarnings("rawtypes")
	public List getAllXs(String hql,int page, int pageSize)// 查询所有学生 分页  
	    {  
	        List list = this.pageShowDao.getAllXs(hql,page, pageSize);  
	        return list;  
	    }  
	  
	    public int findXsSize() {// 所有学生个数  
	        // TODO Auto-generated method stub  
	        return this.pageShowDao.findXsSize();  
	    }  
	
}
