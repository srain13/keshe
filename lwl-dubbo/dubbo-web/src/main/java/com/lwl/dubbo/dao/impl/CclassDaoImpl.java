package com.lwl.dubbo.dao.impl;

import org.springframework.stereotype.Repository;

import com.lwl.dubbo.dao.ICclassDao;
import com.lwl.dubbo.domain.Cclass;



@Repository("cclassDao")
public class CclassDaoImpl extends BaseDaoImpl<Cclass> implements ICclassDao{

}
