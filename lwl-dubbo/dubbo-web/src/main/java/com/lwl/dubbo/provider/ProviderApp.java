package com.lwl.dubbo.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ProviderApp {
	public static void main(String[] args) throws Exception {
		// 读取配置文件
		new ClassPathXmlApplicationContext(new String[] { "dubbo_config.xml" });
		System.out.println("provider服务已注册");
		// 使线程阻塞
		System.in.read();
	}
}
