package com.lwl.dubbo.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.lwl.dubbo.dao.IBaseDao;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

import javax.annotation.Resource;

@Repository("baseDao")
public class BaseDaoImpl<T> implements IBaseDao<T> {

	private SessionFactory sessionFactory;
	protected Session session;
	private Class<T> clazz;

	@SuppressWarnings("unchecked")
	public BaseDaoImpl() {
		
		// 当前对象的直接超类的 Type
		Type genericSuperclass = getClass().getGenericSuperclass();
		if (genericSuperclass instanceof ParameterizedType) {
			// 参数化类型
			ParameterizedType parameterizedType = (ParameterizedType) genericSuperclass;
			// 返回表示此类型实际类型参数的 Type 对象的数组
			Type[] actualTypeArguments = parameterizedType
					.getActualTypeArguments();
			this.clazz = (Class<T>) actualTypeArguments[0];
		} else {
			this.clazz = (Class<T>) genericSuperclass;
		}
	}

	public Class<T> getCls() {
		System.out.println("------> clazz = " + clazz);
		return clazz;
	}

	/**
	 * 通过spring注入sessionFactory
	 * 
	 * @param sessionFactory
	 */
	@Resource(name = "sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	/**
	 * 获取session
	 * 
	 * @return
	 */
	public Session getSession() {
		return sessionFactory.openSession();
	}

	/**
	 * 添加数据
	 */
	public void add(T t) {
		Session mySession = this.getSession();
		try{
			mySession.save(t);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			mySession.flush();		//一定要清除缓存，不然无法删除
			mySession.close();		//先清除缓存再关闭session
		}

	}

	/**
	 * 删除数据
	 */
	public void delete(Serializable id) {
		Session mySession = this.getSession();
		try{
			System.out.println(id+"-------删除的方法执行了----------");			
			mySession.delete(mySession.get(getCls(), id));
		}catch(Exception e){
			e.printStackTrace();
		}finally{			
			mySession.flush();		//一定要清除缓存，不然无法删除
			mySession.close();		//先清除缓存再关闭session
		}

	}

	/**
	 * 更新数据
	 */
	public void update(T t) {
		Session mySession = this.getSession();
		try{
			mySession.update(t);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			mySession.flush();
			mySession.close();		
		}
	}

	/**
	 * 根据id查询某用户
	 */
	@SuppressWarnings("unchecked")
	public T load(Serializable id) {
		Session mySession = this.getSession();
		T t = null;
		try{
		t = (T)mySession.get(getCls(), id);
		System.out.println("load("+id+")----->" + getCls());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			mySession.flush();
			mySession.close();	
		}
		return t;
	}

	/**
	 * 查询一组数据
	 */
	@SuppressWarnings("unchecked")
	public List<T> list(String hql, Object[] args) {
		Session mySession = this.getSession();
		List<T> tList = new ArrayList<T>();
		try{
		Query q = mySession.createQuery(hql);
		if (args != null) {
			for (int i = 0; i < args.length; i++) {
				q.setParameter(i, args[i]);
			}
		}
		tList = q.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			mySession.flush();
			mySession.close();	
		}
		return tList;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List queryBySql(String hql) {
		Session mySession = this.getSession();
		List<Object[]> tList = new ArrayList<Object[]>();
		try{
		tList = mySession.createSQLQuery(hql).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			mySession.flush();
			mySession.close();	
		}
		return tList;
	}
	
	@Override
	public void insertBySql(String sql){
		Session mySession = this.getSession();
			
		try{
			mySession.createSQLQuery(sql).executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			mySession.flush();
			mySession.close();	
		}
	}
	
	
}
