package com.lwl.dubbo.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;


import com.lwl.dubbo.dao.impl.StudentDaoImpl;
import com.lwl.dubbo.domain.Student;
import com.lwl.dubbo.domain.Teacher;
import com.lwl.dubbo.domain.Toptic;
import com.lwl.dubbo.service.IStudentService;
import com.lwl.dubbo.service.ITeacherService;
import com.lwl.dubbo.service.ITopticService;



@Service("studentService")
public class StudentServiceImpl extends BaseServiceImpl<Student> implements
		IStudentService {

	@Resource(name = "studentDao")
	protected StudentDaoImpl studentDao;
	@Resource
	private ITeacherService teacherService;
	@Resource
	private ITopticService topticService;
	private String forwardPage = "";

	
	public void setStudentDao(StudentDaoImpl studentDao) {
		this.studentDao = studentDao;
		super.setBaseDao(studentDao);
	}

	/**
	 * 跳转到选择题的页面
	 */
	public Map<String, String> forwardChoiceTopticPage(String username,String role) {
		
		List<Teacher>list = null;
		Map<String, String> tchMap = null;
		
		if (StringUtils.isNotBlank(role) && !"admin".equals(role)) {
			list = teacherService.list("from Teacher ", null);
			tchMap = new HashMap<String, String>(); // key:教师编号，value:教师名字
			for (Teacher teacher : list) {
				tchMap.put(teacher.getTchId(), teacher.getTchName());
			}			
		} else {
			System.out.println("管理员不参与选题");
		}				
		return tchMap;		
	}
	
	public List<Teacher> getTeacherList(){
		List<Teacher> list = teacherService.list("from Teacher ", null);
		return list;
	}
	
	public String choiceToptic(String username, Toptic toptic,Map<String, Object> session) {
		List<Toptic> list = topticService.list(
				"from Toptic where tsid = '" + username + "'", null);
	if (list.size() > 0) {
		session.put("Error", "您已存在选题，不能再次选择！");
		forwardPage = "Toptic_toChoice.action";
	} else{			
		String sql = "INSERT INTO toptic(topName,topDate,status,tsid,ttid) VALUES('"+toptic.getTopName()+"','"+toptic.getTopDate()+"','"+toptic.getStatus()+"','"+username+"','"+toptic.getTeacher().getTchId()+"')";
		topticService.insertBySql(sql);       
		forwardPage = "Toptic_query.action";
		}
		System.out.println("choiceToptic-------------"+forwardPage);
		return forwardPage;
	}
	
	public String getForwardPage() {
		return forwardPage;
	}

	public void setForwardPage(String forwardPage) {
		this.forwardPage = forwardPage;
	}

}
