package com.lwl.dubbo.dao;

import com.lwl.dubbo.domain.Teacher;

public interface ITeacherDao extends IBaseDao<Teacher> {

}
