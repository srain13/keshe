package com.lwl.dubbo.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lwl.dubbo.dao.impl.BaseDaoImpl;
import com.lwl.dubbo.service.IBaseService;



@Service("baseService")
@Transactional
public class BaseServiceImpl<T> implements IBaseService<T>{
	
	@Resource(name = "baseDao")
	protected BaseDaoImpl<T> baseDao;
	
	public void setBaseDao(BaseDaoImpl<T> baseDao) {
		this.baseDao = baseDao;
	}

	@Override
	public void add(T t) {
		baseDao.add(t);		
	}

	@Override
	public void delete(Serializable id) {
		baseDao.delete(id);
		
	}

	@Override
	public void update(T t) {
		baseDao.update(t);		
	}

	@Override
	public T load(Serializable id) {
		return baseDao.load(id);
	}

	@Override
	public List<T> list(String hql, Object[] args) {
		System.out.println("**************");
		return baseDao.list(hql, args);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List queryBySql(String hql) {
		List<Object[]> list = baseDao.queryBySql(hql);
		return list;
	}

	@Override
	public void insertBySql(String sql) {
		baseDao.insertBySql(sql);
	}




}
