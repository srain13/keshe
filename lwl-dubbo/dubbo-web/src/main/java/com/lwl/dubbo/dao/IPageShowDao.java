package com.lwl.dubbo.dao;

import java.util.List;

import com.lwl.dubbo.domain.PageShow;




public interface IPageShowDao extends IBaseDao<PageShow> {

	@SuppressWarnings("rawtypes")
	public List getAllXs(String hql,int pageNow, int pageSize);
	
}
