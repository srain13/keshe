package com.lwl.dubbo.dao.impl;

import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.lwl.dubbo.dao.ITopticDao;
import com.lwl.dubbo.domain.Toptic;



@Repository("topticDao")
public class TopticDaoImpl extends BaseDaoImpl<Toptic> implements ITopticDao{

	public Toptic queryTopticInfo(String tchId, Integer topId) {
		Session session = this.getSession();
		 String hql = "from Toptic where id=? and ttid=?";    

         Query query = session.createQuery(hql);   
         
         query.setParameter(0, topId);    

         query.setParameter(1, tchId);    

         List<Toptic> list = query.list();   
         
		return list.get(0);
	}

}

