package com.lwl.dubbo.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lwl.dubbo.dao.impl.CclassDaoImpl;
import com.lwl.dubbo.domain.Cclass;
import com.lwl.dubbo.service.ICclassService;




@Service("cclassService")
public class CclassServiceImpl extends BaseServiceImpl<Cclass> implements ICclassService{

	@Resource(name = "cclassDao")
	protected CclassDaoImpl cclassDao;

	
	public void setCclassDao( CclassDaoImpl cclassDao) {
		this.cclassDao = cclassDao;
		super.setBaseDao(cclassDao);
	}
	
	
}
