package com.lwl.dubbo.dao.impl;

import org.springframework.stereotype.Repository;

import com.lwl.dubbo.dao.IStudentDao;
import com.lwl.dubbo.domain.Student;



@Repository("studentDao")
public class StudentDaoImpl extends BaseDaoImpl<Student> implements IStudentDao{

}
