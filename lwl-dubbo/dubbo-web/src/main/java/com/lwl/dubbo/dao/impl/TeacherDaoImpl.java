package com.lwl.dubbo.dao.impl;

import org.springframework.stereotype.Repository;

import com.lwl.dubbo.dao.ITeacherDao;
import com.lwl.dubbo.domain.Teacher;





@Repository("teacherDao")
public class TeacherDaoImpl extends BaseDaoImpl<Teacher> implements ITeacherDao{

}
