package com.lwl.dubbo.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lwl.dubbo.dao.impl.UserDaoImpl;
import com.lwl.dubbo.domain.User;
import com.lwl.dubbo.service.IUserService;



@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<User> implements IUserService{

	@Resource(name = "userDao")
	protected UserDaoImpl userDao;

	public void setUserDao(UserDaoImpl userDao) {
		this.userDao = userDao;
		super.setBaseDao(userDao);
	}
	
	
}
