package com.lwl.dubbo.service.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lwl.dubbo.dao.impl.TopticDaoImpl;
import com.lwl.dubbo.domain.Toptic;
import com.lwl.dubbo.service.ITopticService;



@Service("topticService")
public class TopticServiceImpl extends BaseServiceImpl<Toptic> implements ITopticService{
	
	@Resource(name = "topticDao")
	protected TopticDaoImpl topticDao;


	public void setTopticDao(TopticDaoImpl topticDao) {
		this.topticDao = topticDao;
		super.setBaseDao(topticDao);
	}


	@Override
	public Toptic queryTopticInfo(String tchId, Integer topId) {
		Toptic toptic = topticDao.queryTopticInfo(tchId, topId);
		return toptic;
	}
	
	

}
