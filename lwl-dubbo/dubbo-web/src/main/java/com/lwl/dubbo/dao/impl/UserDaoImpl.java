package com.lwl.dubbo.dao.impl;

import org.springframework.stereotype.Repository;

import com.lwl.dubbo.dao.IUserDao;
import com.lwl.dubbo.domain.User;



@Repository("userDao")
public class UserDaoImpl extends BaseDaoImpl<User> implements IUserDao{

}
