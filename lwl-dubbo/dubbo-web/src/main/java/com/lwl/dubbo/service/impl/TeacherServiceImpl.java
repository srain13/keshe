package com.lwl.dubbo.service.impl;


import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lwl.dubbo.dao.impl.TeacherDaoImpl;
import com.lwl.dubbo.domain.Teacher;
import com.lwl.dubbo.service.ITeacherService;
import com.lwl.dubbo.service.ITopticService;





@Service("teacherService")
public class TeacherServiceImpl extends BaseServiceImpl<Teacher> implements ITeacherService{
	
	@Resource
	private ITopticService topticService;
	
	@Resource(name = "teacherDao")
	protected TeacherDaoImpl teacherDao;

	public void setTeacherDao(TeacherDaoImpl teacherDao) {
		this.teacherDao = teacherDao;
		super.setBaseDao(teacherDao);
	}
	
	/**
	 * 修改选题状态
	 */

	@Override
	public void modifyTopticStatue(int id, String role ,String status) {
		// TODO Auto-generated method stub
		if (null != status && "confirm".equals(status)
				&&"teacher".equals(role)) {
			String sql="UPDATE toptic SET status = '已通过' WHERE id = '" + id + "'";
			topticService.insertBySql(sql);
		} else if (null != status && "reject".equals(status)
				&&  "teacher".equals(role)) {
			String sql="UPDATE toptic SET status = '未通过' WHERE id = '" + id + "'";
			topticService.insertBySql(sql);
		}
	}	
}

