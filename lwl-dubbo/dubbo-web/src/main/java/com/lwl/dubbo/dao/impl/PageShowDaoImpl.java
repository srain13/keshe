package com.lwl.dubbo.dao.impl;

import java.util.List;  

import org.hibernate.Query;  
import org.hibernate.Session;

import com.lwl.dubbo.dao.IPageShowDao;
import com.lwl.dubbo.domain.PageShow;  





@SuppressWarnings("rawtypes")
public class PageShowDaoImpl extends BaseDaoImpl<PageShow> implements IPageShowDao{

	public List getAllXs(String hql,int pageNow, int pageSize){
		// 查询所有学生 分页
        Session session = getSession();//父类方法   
        Query query = session.createQuery(hql);//执行查询操作  
        query.setFirstResult((pageNow - 1) * pageSize);  
        query.setMaxResults(pageSize);  
        List list = query.list();  
        session.close();  
        session=null; 
        
        if (list.size() > 0) {  
            return list;  
        }    
        return null;  
    }  
    public int findXsSize() {// 所有学生个数  
        Session session = getSession();  
        String hql = "from Student";  
        int size = session.createQuery(hql).list().size();        
        session.close();  
        return size;  
    }  
	  
}
