package com.lwl.dubbo.dao;

import com.lwl.dubbo.domain.User;

public interface IUserDao extends IBaseDao<User> {

}
