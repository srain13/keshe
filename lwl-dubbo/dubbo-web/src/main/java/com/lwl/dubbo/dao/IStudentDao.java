package com.lwl.dubbo.dao;

import com.lwl.dubbo.domain.Student;

public interface IStudentDao extends IBaseDao<Student> {

}
