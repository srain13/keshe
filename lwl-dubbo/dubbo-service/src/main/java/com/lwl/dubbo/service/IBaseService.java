package com.lwl.dubbo.service;

import java.io.Serializable;
import java.util.List;

public interface IBaseService<T> {
	 public void add(T t);  
	    
	    public void delete(Serializable id);  
	  
	    public void update(T t);  
	     
	    public T load(Serializable id);  		//	查询不需要事务管理
	     
	    public List<T> list(String hql, Object[] args); 
	    
	    @SuppressWarnings("rawtypes")
		public List queryBySql(String hql);
	    
	    public void insertBySql(String sql);
}
