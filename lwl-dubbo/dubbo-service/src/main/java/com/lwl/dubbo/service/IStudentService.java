package com.lwl.dubbo.service;

import java.util.List;
import java.util.Map;

import com.lwl.dubbo.domain.Student;
import com.lwl.dubbo.domain.Teacher;
import com.lwl.dubbo.domain.Toptic;





public interface IStudentService extends IBaseService<Student>{
	/**
	 * 跳转到选题的页面
	 * @param username	用户名
	 * @param role		角色
	 * @return
	 */
	public Map<String, String> forwardChoiceTopticPage(String username,String role);
	
	public String choiceToptic(String username,Toptic toptic,Map<String, Object> session);
	
	public List<Teacher> getTeacherList();
}
