package com.lwl.dubbo.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * Teacher entity. @author MyEclipse Persistence Tools
 */

public class Teacher implements java.io.Serializable {

	// Fields

	private String tchId;
	private Cclass cclass;
	private String tchName;
	private String tchSex;
	private String subject;
	private String password;
	private Set toptics = new HashSet(0);

	// Constructors

	/** default constructor */
	public Teacher() {
	}

	/** full constructor */
	public Teacher(Cclass cclass, String tchName, String tchSex, String subject, String password, Set toptics) {
		this.cclass = cclass;
		this.tchName = tchName;
		this.tchSex = tchSex;
		this.subject = subject;
		this.password = password;
		this.toptics = toptics;
	}

	// Property accessors

	public String getTchId() {
		return this.tchId;
	}

	public void setTchId(String tchId) {
		this.tchId = tchId;
	}

	public Cclass getCclass() {
		return this.cclass;
	}

	public void setCclass(Cclass cclass) {
		this.cclass = cclass;
	}

	public String getTchName() {
		return this.tchName;
	}

	public void setTchName(String tchName) {
		this.tchName = tchName;
	}

	public String getTchSex() {
		return this.tchSex;
	}

	public void setTchSex(String tchSex) {
		this.tchSex = tchSex;
	}

	public String getSubject() {
		return this.subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set getToptics() {
		return this.toptics;
	}

	public void setToptics(Set toptics) {
		this.toptics = toptics;
	}

}