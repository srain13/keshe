package com.lwl.dubbo.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式
 * @author 
 *
 */
public class RegUtil {

	/**
	 * 获取字符串中的数字
	 */
	public static String getDigit(String digit) {
		String num = null;
		String regEx = "\\d+";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(digit);
		if (m.find()) {
			num = m.group(0);
		}
		return num;
	}
	
	public static void main(String[] args) {
		String str = "201500208";
		System.out.println(str.substring(0, 8));
	}

}
