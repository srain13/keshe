package com.lwl.dubbo.service;

import com.lwl.dubbo.domain.Toptic;

public interface ITopticService extends IBaseService<Toptic>{
	/**
	 * 获取课设详情
	 * @param tchId 教师ID
	 * @param topId 课设ID
	 * @return
	 */
	public Toptic queryTopticInfo(String tchId, Integer topId);
}
