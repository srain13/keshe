package com.lwl.dubbo.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * Student entity. @author MyEclipse Persistence Tools
 */

public class Student implements java.io.Serializable {

	// Fields
	private static final long serialVersionUID = 6775434497203811200L;
	private String stuId;
	private Cclass cclass;
	private String stuName;
	private String stuSex;
	private String stuNo;
	private String password;
	private Set toptics = new HashSet(0);

	// Constructors

	/** default constructor */
	public Student() {
	}

	/** full constructor */
	public Student(Cclass cclass, String stuName, String stuSex, String stuNo, String password, Set toptics) {
		this.cclass = cclass;
		this.stuName = stuName;
		this.stuSex = stuSex;
		this.stuNo = stuNo;
		this.password = password;
		this.toptics = toptics;
	}

	// Property accessors

	public String getStuId() {
		return this.stuId;
	}

	public void setStuId(String stuId) {
		this.stuId = stuId;
	}

	public Cclass getCclass() {
		return this.cclass;
	}

	public void setCclass(Cclass cclass) {
		this.cclass = cclass;
	}

	public String getStuName() {
		return this.stuName;
	}

	public void setStuName(String stuName) {
		this.stuName = stuName;
	}

	public String getStuSex() {
		return this.stuSex;
	}

	public void setStuSex(String stuSex) {
		this.stuSex = stuSex;
	}

	public String getStuNo() {
		return this.stuNo;
	}

	public void setStuNo(String stuNo) {
		this.stuNo = stuNo;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set getToptics() {
		return this.toptics;
	}

	public void setToptics(Set toptics) {
		this.toptics = toptics;
	}

}