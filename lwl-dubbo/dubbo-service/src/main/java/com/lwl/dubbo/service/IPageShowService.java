package com.lwl.dubbo.service;



import java.util.List;

import com.lwl.dubbo.domain.PageShow;




@SuppressWarnings("rawtypes")
public interface IPageShowService extends IBaseService<PageShow> {

	public List getAllXs(String hql,int pageNow, int pageSize);// 查询所有学生 分页  
	
    public int findXsSize(); // 所有学生个数  
}
