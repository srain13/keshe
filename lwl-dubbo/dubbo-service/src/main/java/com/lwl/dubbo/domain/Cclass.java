package com.lwl.dubbo.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * Cclass entity. @author MyEclipse Persistence Tools
 */

public class Cclass implements java.io.Serializable {

	// Fields

	private String classId;
	private String claName;
	private Set teachers = new HashSet(0);
	private Set students = new HashSet(0);

	// Constructors

	/** default constructor */
	public Cclass() {
	}

	/** full constructor */
	public Cclass(String claName, Set teachers, Set students) {
		this.claName = claName;
		this.teachers = teachers;
		this.students = students;
	}

	// Property accessors

	public String getClassId() {
		return this.classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getClaName() {
		return this.claName;
	}

	public void setClaName(String claName) {
		this.claName = claName;
	}

	public Set getTeachers() {
		return this.teachers;
	}

	public void setTeachers(Set teachers) {
		this.teachers = teachers;
	}

	public Set getStudents() {
		return this.students;
	}

	public void setStudents(Set students) {
		this.students = students;
	}

}