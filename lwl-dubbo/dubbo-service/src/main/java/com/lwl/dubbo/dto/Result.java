package com.lwl.dubbo.dto;

public class Result {

	private String id;
	
	private String topName;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTopName() {
		return topName;
	}

	public void setTopName(String topName) {
		this.topName = topName;
	}


	
}
