package com.lwl.dubbo.domain;

/**
 * Toptic entity. @author MyEclipse Persistence Tools
 */

public class Toptic implements java.io.Serializable {

	// Fields

	private Integer id;
	private Student student;
	private Teacher teacher;
	private String topName;
	private String topDate;
	private String status;
	private String uploadFileName;
	private String savePath;
	private String fileName;

	// Constructors

	/** default constructor */
	public Toptic() {
	}

	/** full constructor */
	public Toptic(Student student, Teacher teacher, String topName, String topDate, String status,
			String uploadFileName, String savePath, String fileName) {
		this.student = student;
		this.teacher = teacher;
		this.topName = topName;
		this.topDate = topDate;
		this.status = status;
		this.uploadFileName = uploadFileName;
		this.savePath = savePath;
		this.fileName = fileName;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Student getStudent() {
		return this.student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Teacher getTeacher() {
		return this.teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public String getTopName() {
		return this.topName;
	}

	public void setTopName(String topName) {
		this.topName = topName;
	}

	public String getTopDate() {
		return this.topDate;
	}

	public void setTopDate(String topDate) {
		this.topDate = topDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUploadFileName() {
		return this.uploadFileName;
	}

	public void setUploadFileName(String uploadFileName) {
		this.uploadFileName = uploadFileName;
	}

	public String getSavePath() {
		return this.savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}