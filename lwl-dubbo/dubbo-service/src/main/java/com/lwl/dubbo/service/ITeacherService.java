package com.lwl.dubbo.service;

import com.lwl.dubbo.domain.Teacher;

public interface ITeacherService extends IBaseService<Teacher>{

	/**
	 * 修改选题状态
	 * @param id		用于跳转到更新页面
	 * @param username	登陆的用户名
	 * @param status	选题的状态
	 */
	public void modifyTopticStatue(int id,String role,String status);

}
