<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>课程设计选题管理</title>
    
    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- Page-Level Plugin CSS - Dashboard -->
    <link href="style/css/plugins/morris/morris-0.4.3.min.css" rel="stylesheet">
    <link href="style/css/plugins/timeline/timeline.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <a class="navbar-brand" href="index.html">课程设计选题管理</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="main.jsp">
                        <i class="fa fa-user fa-fw"></i> <label>${username}</label>
                    </a>

                    <!-- /.dropdown-user -->
                </li>
                <li>
                   <a class="dropdown-toggle" data-toggle="dropdown" href="User_logout.action" target="_parent" onclick="return confirm('确认退出？')">
                        <i class="fa fa-sign-out fa-fw"></i> <label>退出</label>
                    </a>
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
        </nav>

      
        </div>
    <!-- /#wrapper -->

</body>

</html>
