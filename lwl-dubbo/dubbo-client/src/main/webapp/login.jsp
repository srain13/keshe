<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>软件工程课程设计信息管理系统登陆</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">
    
    <style type="text/css">
    input,img{vertical-align:middle;}
    </style>
    <script type="text/javascript">       
      function reloadcode(obj,base){    
      var rand=new Date().getTime();     
      obj.src=base+"validCode.action?abc="+rand;       
      }       
    </script> 

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">登陆</h3>
                    </div>
                    <div class="panel-body">
                    	<div style="padding:5px;text-align:center;color: red;">${verifyCodeError}</div>
                        <form action="<%=basePath%>User_login.action" method="post">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="用户名" id="username" name="username" type="text" value="">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="密码" id="password" name="password" type="password" value="">
                                </div>
								<div class="form-group" >
                                    <input style="width:150px;float:left;" maxlength="4" class="form-control" placeholder="验证码" id="chknumber" name="chknumber" type="text" value="">   
                                    <img title="看不清楚请点击这里" style="float:right;"  width="150" height="35" src="<%=basePath%>validCode.action" id="safecode" onclick="reloadcode(this,'<%=basePath%>')" />                                 
                                </div>
								<div class="form-group" style="margin: 5px;float:left;">
                                            <label class="radio-inline">
                                                <input name="role" id="student" value="student" checked="" type="radio">学生
                                            </label>
                                            <label class="radio-inline">
                                                <input name="role" id="teacher" value="teacher" type="radio">教师
                                            </label>
                                            <label class="radio-inline">
                                                <input name="role" id="admin" value="admin" type="radio">管理员
                                            </label>
                                </div>
                                <input type="submit" class="btn btn-lg  btn-info btn-block" value="登陆"></input>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>
