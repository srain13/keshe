<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>毕业设计选题管理</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
	<!--图标-->
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">

</head>

<body>

    <div id="wrapper">

        <div class="navbar-default navbar-static-side" role="navigation" style="margin: 0px;width:200px">
           <!-- <div class="sidebar-collapse">-->
                <ul class="nav" id="side-menu">

                    <li>
                        <a href="" target="mainFrame"><i class="fa fa-dashboard fa-fw"></i> 主页</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-th-large fa-fw"></i> 信息管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
				
                            <li>
                                <a href="Teacher_queryTeacher.action" target="mainFrame">教师信息</a>
                            </li>
                            <li>
                                <a href="PageShow_getAllXs.action" target="mainFrame">学生信息</a>
                            </li>
				<li>
                                <a href="Teacher_queryToptic.action" target="mainFrame">选题信息</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-plane fa-fw"></i> 选题管理<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
				<li>
                                <a href="Toptic_query.action" target="mainFrame">选题信息</a>
                            </li>
                            <li>
                                <a href="Toptic_toChoice.action" target="mainFrame">学生选题</a>
                            </li>
                            <li>
                                <a href="Toptic_toAdd.action" target="mainFrame">发布选题</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li> 
                    <li>
                        <a href="error.jsp" target="mainFrame"><i class="fa fa-square-o fa-fw" ></i> 帮助</a>
                    </li>
                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </div>
	<!--</div>-->
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
        <script src="style/js/jquery-1.8.2.js"></script>
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="style/js/sb-admin.js"></script>


</body>

</html>
