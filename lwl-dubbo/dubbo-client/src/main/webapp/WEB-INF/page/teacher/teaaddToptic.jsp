<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>发布课题</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">
       
  </head>
  
  <body>
    <div class="container" style="margin-left: 10px;padding: 5px;">
 <div class="row" >
	<div class="panel-body">
		<form action="Upload_execute.action" method="post"  enctype="multipart/form-data">
	    <div class="form-group" >
	    <label>课题名称</label>
            <input id="toptic.topName" class="form-control" placeholder="课题名称" name="toptic.topName" type="text" style="width:200px;">
        </div>
        <br>
       <div class="form-group" >
	    <label>上传课题文件</label>
        <input type="file" id="dofile" name="file"/> 
        </div>                        
                                 
	<input type="submit" name="" class="btn btn-primary" value="保存" style="margin-bottom:10px"/>				
	</form>
	</div>
	</div>
</div>
    <!-- Core Scripts - Include with every page -->
    <script src="style/js/jquery-1.8.2.js"></script>
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="style/js/sb-admin.js"></script>
  </body>
</html>
