<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>课题信息</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <!--图标-->
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">
        <script type="text/javascript">
	window.onload=function(){
		
		function selectQuery(){
			document.getElementById("status").value=document.getElementById("queryList").value;
		}
		selectQuery();
		
		  $("#queryList").change(function(){
			  var status = document.getElementById("queryList").value;		  
			  document.getElementById("status").value = status;
		 });
		  
	} 
	</script>

</head>

<body>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
         <div style="width:200px;" class="form-group">		
              <select id="queryList"  style="width:150px;float:left;margin-bottom:10px;" class="form-control" onchange="selectQuery()">               
				<option value="未确认">未确认</option>
				<option value="未审批">未审批</option>				
              </select>
           <form action="Teacher_queryTopticByStatus.action" method="post">
           <input type="hidden" id="status" name="status" value=""/> 
           <input type="submit" name="" class="btn-primary" value="查询" style="height:32px;float:right;"/>
           </form>
</div>      
                        <table class="table table-striped table-bordered table-hover" >
                            <thead>
                                <tr>
                                    <th>编号</th>
                                    <th>课题名称</th>
                                    <th>学生姓名</th>
                                    <th>教师姓名</th>
                                    <th>文件名称</th>
                                    <th>文件路径</th>                                    
									<th>状态</th>
									<th>日期</th>
									<th>操作</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<c:forEach items="${list}" var="list">
                              <tr>
                                <td>${list.id} </td>
                                <td>${list.topName}</td>
                                <td>${list.student.stuName}</td>
								<td>${list.teacher.tchName}</td>
								<td>${list.fileName}</td>
								<td>${list.savePath}</td>								
								<td>${list.status}</td>
								<td>${list.topDate}</td>
								<td>
	<a href="Teacher_topticStatus?status=confirm&id=${list.id}">确认</a> 
	<a href="Teacher_topticStatus?status=reject&id=${list.id}" onclick="return confirm('确认要拒绝？')">拒绝</a>
	<a href="Teacher_toUpdateToptic?id=${list.id}">修改</a> 
	</td>

                              </tr>
                              </c:forEach>

                          </tbody>
                      </table>
                  </div>
                  <!-- /.table-responsive -->
              </div>
              <!-- /.panel-body -->
          </div>
                    <!-- /.panel -->
                </div>
            </div>
            <!-- /.row -->

    <!-- Core Scripts - Include with every page -->
      <script src="style/js/jquery-1.8.2.js"></script>
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="style/js/sb-admin.js"></script>

</body>

</html>
