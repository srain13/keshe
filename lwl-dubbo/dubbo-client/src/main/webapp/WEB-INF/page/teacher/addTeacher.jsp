<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.text.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>增加教师</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">
	<script type="text/javascript">
	window.onload=function(){
		
		  $("#sexList"). change(function(){
			  var tchSex = document.getElementById("sexList").value;
			  document.getElementById("teacher.tchSex").value = tchSex;			 
		 });
		  
		function selectSex(){
			document.getElementById("teacher.tchSex").value=document.getElementById("sexList").value;
		}
		selectSex();
	} 
	</script>
</head>

<body>
 <div class="container" style="margin-left: 10px;padding: 5px;">
 <div class="row" >
	<div class="panel-body">
		<form action="Teacher_add.action" method="post">
	    <div class="form-group" >
            <input id="teacher.tchName" class="form-control" placeholder="姓名" name="teacher.tchName" type="text" style="width:200px;">
        </div>
		<div class="form-group" style="width:200px;">
                <select id="sexList" class="form-control" onchange="selectSex()">
                    <option value="男" >男</option>
					<option value="女">女</option>
                </select>
        </div>
        <input type="hidden" id="teacher.tchSex" name="teacher.tchSex"/>
        
     <div class="form-group" >
            <input id="teacher.subject" class="form-control" placeholder="教学课目" name="teacher.subject" type="text" style="width:200px;">
        </div>   
        
   <input type="submit" name="" class="btn btn-primary" value="保存" style="margin-bottom:10px"/>					
	</form>
	</div>
	</div>
</div>

    <!-- Core Scripts - Include with every page -->
    <script src="style/js/jquery-1.8.2.js"></script>
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="style/js/sb-admin.js"></script>

</body>

</html>
