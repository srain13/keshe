<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>教师信息</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <!--图标-->
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">

</head>

<body>

	  <div class="row">
	      <div class="col-lg-12">
	          <div class="panel panel-default">
	
	              <!-- /.panel-heading -->
	              <div class="panel-body">
	              <div style="margin: 6px">
	              <form action="Teacher_queryTeacherByID.action" method="post">
	 				<label>教师编号：</label><input type="text" id="teacher.tchId" name="teacher.tchId" value="" /> 
	 				<input type="submit" name="" class="btn btn-primary" value="查询" style="height:30px;"/>
	 				<a href="Teacher_toAdd.action" style="height:30px;" class="btn btn-primary">新增</a>
	 				</form>
	 				</div>
	                  <div class="table-responsive">
	                      <table class="table table-striped table-bordered table-hover" >
	                          <thead>
	                              <tr>
	                                  <th>编号</th>
	                                  <th>姓名</th>
	                                  <th>性别</th>
	                                  <th>教学课程</th>
	                                	<th>操作</th>
	                              </tr>
	                          </thead>
	                          <tbody>
	                          	<c:forEach items="${list}" var="list">
	                              <tr>
	                                  <td>${list.tchId}</td>
	                                  <td>${list.tchName}</td>
	                                  <td>${list.tchSex}</td>
		<td>${list.subject}</td>
		<td>
		<a href="Teacher_toUpdate.action?teacher.tchId=${list.tchId}">更新</a>
		<a href="Teacher_delete.action?teacher.tchId=${list.tchId}" onclick="return confirm('确认要删除？')">删除</a>
		</td>
	
	                              </tr>
	                              </c:forEach>
	
	                          </tbody>
	                      </table>
	                  </div>
	                  <!-- /.table-responsive -->
	              </div>
	                        <!-- /.panel-body -->
	                    </div>
	                    <!-- /.panel -->
	                </div>
	            </div>
	            <!-- /.row -->
	
	    <!-- Core Scripts - Include with every page -->
        <script src="style/js/jquery-1.8.2.js"></script>
	    <script src="style/js/bootstrap.min.js"></script>
	    <script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	
	    <!-- SB Admin Scripts - Include with every page -->
	    <script src="style/js/sb-admin.js"></script>

</body>

</html>
