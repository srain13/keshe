<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.text.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>选择课题</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">
    

</head>

<body>
 <div class="container" style="margin-left: 10px;padding: 5px;">
 <div class="row" >
	<div class="panel-body">
	<div style="padding:5px;text-align:center;color: red;">${Error}</div>
		<form action="Toptic_choice.action" method="post">
	<div  class="form-group" style="width:200px;" >
		<label>老师</label>
                <select id="tchList"  class="form-control" onchange=""> 
                	<option value="">请选择科任老师</option>  
                	<c:forEach var="tea" items="${teacher}">
                		<option value="${tea.tchId}">${tea.tchName}</option>
              	   </c:forEach>            
                </select>
        </div>
      <input type="hidden" id="tchId"   name="tchId"  value="" /> 
            
		<div class="form-group" style="width:200px;">
		<label>课程题目</label>
			    <select id="topList"  class="form-control" >
                    <option value="">请选择题目</option> 
                </select>
        </div>
     
        <input type="hidden" id="topName" name="topName" value=""/>
        
        <input type="hidden" id="stuId" name="stuId" value=""/>
       
        <input type="hidden" id="toptic.status" name="toptic.status" value="未确认"/>

		<div class="form-group">
		<label>录入时间</label>
            <input class="form-control" readonly="readonly" placeholder="录入时间" id="toptic.topDate" name="toptic.topDate" type="text" value="<%=formatter.format(new Date())%>" style="width:200px;">
		</div>
		<input type="submit" onclick="getTopName()" name="" class="btn btn-primary" value="保存" style="margin-bottom:10px"/>			
	</form>
	
	<div id="disDownload" class="form-group" style="width:200px;display: none">
			<label>课程要求</label>
				    <button  onclick="download()">点击下载</button>
	        </div>
		</div>
	</div>
</div>

    <!-- Core Scripts - Include with every page -->
    <script src="style/js/jquery-1.8.2.js"></script>
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="style/js/sb-admin.js"></script>

</body>
<script type="text/javascript">

 window.onload = function() {	 
	 var selectChange = function() {
		 var selectFrist = document.querySelector('#tchList');
		 selectFrist.addEventListener('change', function(event) {
			 var val = $("#tchList").val();
			 $("#tchId").val(val);
			 var url = 'Toptic_chiceCourse.action';
			 $.ajax({
				 url : url,
				 type : 'POST', // GET
				 async : false, // 或false,是否异步
				 data : {
				 'tchId' : val
				 },
				 timeout : 5000, // 超时时间
				 dataType : 'json', // 返回的数据格式：json/xml ml/script/jsonp/text
				 success : function(data) {
					 var number = data.length;
					 if(number!=0){
						 // 获取dom结构
						 var topList = document.querySelector('#topList');
						 // 给个空的变量
						 var html = '';
						 data.forEach(function(i) {
							 html += '<option value="'+i.id+'">'+i.topName+'</option>';
						 })
						 topList.innerHTML = html; 
						 $("#disDownload").show();
					 }else{
						 // 获取dom结构
						 var topList = document.querySelector('#topList');
						 // 给个空的变量
						 var html = '<option value="">该老师还没有发布课题</option>';
						 topList.innerHTML = html;
						 $("#disDownload").hide();
					 }
				 },
				 error : function(xhr, textStatus) {
				 alert('错误' + textStatus);
				 }
				 });
		 });
	 }();
 }

 function getTopName(){	
	 $("#topName").val($("#topList").find("option:selected").text());
 }
 
 var download = function(){
	var tchId =  $("#tchList").val();
	var topId =  $("#topList").val();
	 var url = 'Download_execute.action';
	 //这里不能使用ajax发起请求因为没有回调
	 window.location.href="Download_myExecute.action?tchId="+tchId+"&topId="+topId;
// 	 $.ajax({
// 		 url : url,
// 		 type : 'POST', // GET
// 		 async : false, // 或false,是否异步
// 		 data : {
// 		 'tchId' : tchId,
// 		 'topId' : topId
// 		 },
// 		 timeout : 5000, // 超时时间
// 		 dataType : 'json', // 返回的数据格式：json/xml ml/script/jsonp/text
// 		 success : function(data) {
			 
// 		 }
// 	 });
 }
</script>
</html>
