<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>修改课题</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">
    <script type="text/javascript">
	window.onload=function(){
		
		function selectTch(){
			document.getElementById("toptic.teacher.tchId").value=document.getElementById("tchList").value;
		}
		selectTch();
		
		  $("#tchList").change(function(){
			  var t_tch_code = document.getElementById("tchList").value;
			  document.getElementById("toptic.teacher.tchId").value = t_tch_code;
			  
			  document.getElementById("toptic.status").value = "未确认";
		 });
		  
	} 
	</script>
</head>

<body>
 <div class="container" style="margin-left: 10px;padding: 5px;">
 <div class="row" >
	<div class="panel-body">
	<div style="padding:5px;text-align:center;color: red;">${Error}</div>
		<form action="Toptic_updateToptic.action" method="post">
	    <div class="form-group" >
	    <label>课题名称</label>
            <input id="toptic.TId" class="form-control" value="${toptic.topName}" placeholder="课题名称" name="toptic.topName" type="text" style="width:200px;">
        </div>
        <div class="form-group" style="width:200px;">		
		<label>选择教师</label>
                <select id="tchList" class="form-control" onchange="selectTch()">               
                <c:forEach var="map" items="${tchMap}">
				<option value="${map.key}">${map.value}</option>
				</c:forEach>				
                </select>
        </div>       
        <input type="hidden" id="toptic.teacher.tchId" name="toptic.teacher.tchId" value="${toptic.teacher.tchId}"/>   
        <input type="hidden" id="toptic.id" name="toptic.id" value="${toptic.id}"/> 
        <input type="hidden" id="toptic.status" name="toptic.status" value="${toptic.status}"/> 
             	    
		<input type="submit" name="" class="btn btn-primary" value="保存" style="margin-bottom:10px"/>			
	</form>
	</div>
	</div>
</div>

    <!-- Core Scripts - Include with every page -->
     <script src="style/js/jquery-1.8.2.js"></script>
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="style/js/sb-admin.js"></script>

</body>

</html>
