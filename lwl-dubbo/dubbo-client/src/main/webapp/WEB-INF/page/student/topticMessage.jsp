<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>课题信息</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <!--图标-->
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">

</head>

<body>

     <div class="row">
         <div class="col-lg-12">
             <div class="panel panel-default">

                 <!-- /.panel-heading -->
                 <div class="panel-body">
                     <div class="table-responsive">
                         <table class="table table-striped table-bordered table-hover" >
                             <thead>
                                 <tr>
                                     <th>编号</th>
                                     <th>课题名称</th>
                                     <th>学生姓名</th>
                                     <th>教师姓名</th>
				<th>状态</th>
				<th>日期</th>
				<th>操作</th>
                                 </tr>
                             </thead>
                             <tbody>
                             	<c:forEach items="${list}" var="list">
                                 <tr>
                                     <td>${list.id}</td>
                                     <td>${list.topName}</td>
                                     <td>${list.student.stuName}</td>
				<td>${list.teacher.tchName}</td>
				<td>${list.status}</td>
				<td>${list.topDate}</td>
				<td>
				<a href="Toptic_toUpdateToptic?id=${list.id}">更新</a> 
				<a href="Toptic_delete.action?toptic.id=${list.id}" onclick="return confirm('确认要删除？')">删除</a>
				</td>

                                 </tr>
                                 </c:forEach>
   
                             </tbody>
                         </table>
                     </div>
                     <!-- /.table-responsive -->
                 </div>
                 <!-- /.panel-body -->
             </div>
             <!-- /.panel -->
         </div>
     </div>
     <!-- /.row -->

    <!-- Core Scripts - Include with every page -->
       <script src="style/js/jquery-1.8.2.js"></script>
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="style/js/sb-admin.js"></script>

</body>

</html>
