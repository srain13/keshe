<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ page language="java" import="java.text.*"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>更新学生信息</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">
	<script type="text/javascript">
	window.onload=function(){ 		
		  $("#sexList").change(function(){
			  var stuSex = document.getElementById("sexList").value;
			  document.getElementById("student.stuSex").value = stuSex;			 
		 });
				  
		function selectSex(){
			document.getElementById("student.stuSex").value=document.getElementById("sexList").value;
		}
		selectSex();				
	}

	</script>
</head>

<body>
 <div class="container" style="margin-left: 10px;padding: 5px;">
 <div class="row" >
	<div class="panel-body">
		<form action="Student_update.action" method="post">
	    <div class="form-group" >	    
	    <label>姓名</label>
            <input id="student.stuName" class="form-control" placeholder="姓名" name="student.stuName" type="text" style="width:200px;">
        </div>
        
		<div class="form-group" style="width:200px;">
		<label>性别</label>
                <select id="sexList" class="form-control" onchange="selectSex()">
                    <option value="男" >男</option>
					<option value="女">女</option>
                </select>
        </div>
        <input type="hidden" id="student.stuSex" name="student.stuSex"/>
                    
	
           <div class="form-group" >	  
	    <label>密码</label>
            <input id="student.password" class="form-control" placeholder="密码" name="student.password" type="text" style="width:200px;">
        </div>
          
        <input type="hidden" id="student.stuId" name="student.stuId"  value="${student.stuId}"/>
		<input type="submit" name="" class="btn btn-primary" value="保存" style="margin-bottom:10px"/>			
	</form>
	</div>
	</div>
</div>

    <!-- Core Scripts - Include with every page -->
        <script src="style/js/jquery-1.8.2.js"></script>
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src="style/js/sb-admin.js"></script>

</body>

</html>
