<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@taglib prefix="s"  uri="/struts-tags"%>  

<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>

<head>
	<base href="<%=basePath%>">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>学生信息</title>

    <!-- Core CSS - Include with every page -->
    <link href="style/css/bootstrap.min.css" rel="stylesheet">
    <!--图标-->
    <link href="style/font-awesome/css/font-awesome.css" rel="stylesheet">

    <!-- SB Admin CSS - Include with every page -->
    <link href="style/css/sb-admin.css" rel="stylesheet">

</head>

<body>

	<div class="row">
	    <div class="col-lg-12">
	        <div class="panel panel-default">
	
	            <!-- /.panel-heading -->
	           <div class="panel-body">
	           <div style="margin: 6px">
	           <form action="Student_queryStudentByName.action" method="post">
			<label>姓名：</label><input type="text" id="stuName" name="stuName" value="" /> 
			<input type="submit" name="" class="btn btn-primary" value="查询" style="height:30px;"/>
			<a href="Student_toAdd.action" style="height:30px;" class="btn btn-primary">新增</a>			
					</form>
					</div>
	                 <div class="table-responsive">
	                     <table class="table table-striped table-bordered table-hover" >
	                         <thead>
	                             <tr>
	                                 <th>学号</th>
	                                 <th>姓名</th>
	                                 <th>性别</th>
	<th>班级</th>
	<th>操作</th>
	                             </tr>
	                         </thead>
	                         <tbody>
	       <c:forEach items="${list}" var="list">
	                           <tr>
	                               <td>${list.stuId}</td>
	                               <td>${list.stuName}</td>
									<td>${list.stuSex}</td>
									<td>${list.stuNo}</td>
									
	<td>
	<a href="Student_toUpdate.action?student.stuId=${list.stuId}"  >更新</a> 
	<a href="Student_delete.action?student.stuId=${list.stuId}"  onclick="return confirm('确认要删除？')">删除</a>
	</td>	
	                             </tr>
	                             </c:forEach>
	                       </tbody>	                       
 	<s:set  var ="page"  value="#request.page"> </s:set>    
        <tr>  
        <td colspan="9">  
        当前是第<s:property value="#page.pageNow"/>页，共<s:property value="#page.totalPage"/>页  
        <s:if test="#page.hasFirst">  
            <a href="PageShow_getAllXs.action?pageNow=1"  >首页</a>  
        </s:if>  
        <s:if test="#page.hasPre">  
            <a href="PageShow_getAllXs.action?pageNow=<s:property value="#page.pageNow-1"/> ">上一页</a>  
        </s:if>  
        <s:if test="#page.hasNext">  
             <a  href="PageShow_getAllXs.action?pageNow=<s:property value="#page.pageNow+1" />" >下一页</a>  
        </s:if>  
         <s:if test="#page.hasLast">  
            <a href="PageShow_getAllXs.action?pageNow=<s:property value="#page.totalPage"/>" >尾页</a>  
        </s:if>  
     </td>  
  </tr>
                         	                      	                       	                                            
	                   </table>
	               </div>
	               <!-- /.table-responsive -->
	           </div>
	           <!-- /.panel-body -->
	       </div>
	       <!-- /.panel -->
	    </div>    
	</div>

	<!-- /.row -->
	
	<!-- Core Scripts - Include with every page -->
	    <script src="style/js/jquery-1.8.2.js"></script>
	<script src="style/js/bootstrap.min.js"></script>
	<script src="style/js/plugins/metisMenu/jquery.metisMenu.js"></script>
	
	<!-- SB Admin Scripts - Include with every page -->
	<script src="style/js/sb-admin.js"></script>

</body>

</html>
