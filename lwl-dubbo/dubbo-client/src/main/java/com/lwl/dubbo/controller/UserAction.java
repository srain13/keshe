package com.lwl.dubbo.controller;


import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.lwl.dubbo.domain.Student;
import com.lwl.dubbo.domain.Teacher;
import com.lwl.dubbo.domain.User;
import com.lwl.dubbo.service.IStudentService;
import com.lwl.dubbo.service.ITeacherService;
import com.lwl.dubbo.service.IUserService;


/**
 * 用户(LOGIN)	ACTION
 * @author
 *
 */
public class UserAction extends BaseAction{

	private static final long serialVersionUID = -6549383410360944356L;

	private String chknumber;	//获取用户输入的验证码	
	private String username;	//用户名
	private String password;	//密码
	private String role;		//获取用户类型：管理员、学生、教师
	@Resource
	private IUserService userService;
	@Resource
	private IStudentService studentService;
	@Resource
	private ITeacherService teacherService;
	

	/**
	 * 
	 * 不同的角色登陆，跳转到不同的视图
	 * @return
	 */
	public String login(){
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpSession session2 = request.getSession();
		//把图片的验证码（生成的都是大写的）和用户输入的验证码都转成大写再进行判断
		boolean isRandomCode = (session.get("randomCode").toString()).equals(chknumber.toUpperCase());
		//判断验证码和角色
        if(isRandomCode&&"admin".equals(role))    
        {    
        	List<User> userList = userService.list("from User where username ='"+username+"' and password = '"+password+"'", null);
        	if(userList.size()>0){
        		session2.setAttribute("username", username);
        		session2.setAttribute("role", role);
        		session2.setAttribute("position", "管理员");
        		forwardPage = "/main.jsp";
        		return SUCCESS;
        	}else{
        		session.put("verifyCodeError", "用户名或者密码错误");
        		forwardPage = "/login.jsp";
        		return ERROR;
        	}               
        }else if(isRandomCode&&"student".equals(role)){   
        	List<Student> studentList = studentService.list("from Student where stuId ='"+username+"' and password = '"+password+"'" ,null);
        	//System.out.println("----2"+studentList.size());
        	if(studentList.size()>0){
        		session2.setAttribute("username", username);
        		session2.setAttribute("role", role);
        		session2.setAttribute("position", "学生");
        		setForwardPage("/main.jsp");
        		return SUCCESS;
        	}else{
        		session.put("verifyCodeError", "用户名或者密码错误");
        		setForwardPage("/login.jsp");
        		return ERROR;
        	} 
        }else if(isRandomCode&&"teacher".equals(role)){
        	List<Teacher> teacherList = teacherService.list("from Teacher where tchId ='"+username+"' and password = '"+password+"'", null);
        	if(teacherList.size()>0){
         		session2.setAttribute("username", username);
        		session2.setAttribute("role", role);
        		session2.setAttribute("position", "老师");
        		setForwardPage("/main.jsp");
        		return SUCCESS;
        	}else{
        		session.put("verifyCodeError", "用户名或者密码错误");
        		setForwardPage("/login.jsp");
        		return ERROR;
        	} 
        }else    
        {    
        	session.put("verifyCodeError", "验证码错误！");
            return ERROR;    
        }
        
	}
	
	/**
	 * 退出登陆
	 * @return
	 */
	public String logout(){
        session.remove("username");
        session.remove("randomCode");
        session.put("verifyCodeError", "退出成功！");
        setForwardPage("/login.jsp");
        return "forwardTchMessage";
	}
			

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getChknumber() {
		return chknumber;
	}


	public void setChknumber(String chknumber) {
		this.chknumber = chknumber;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
