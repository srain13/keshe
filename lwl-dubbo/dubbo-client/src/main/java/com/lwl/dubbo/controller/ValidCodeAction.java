package com.lwl.dubbo.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import com.lwl.dubbo.utils.VerifyCodeUtil;
import com.opensymphony.xwork2.ActionSupport;


/**
 * 验证码		ACTION
 * @author
 *
 */
public class ValidCodeAction extends ActionSupport implements SessionAware,
		ServletResponseAware {

	private Map<String, Object> session;
	private HttpServletResponse response;
	private static final long serialVersionUID = 1L;

	@Override
	public String execute() throws Exception {
		// 请求头可不设置
		response.setHeader("Cache-Control", "no-cache");
		// 生成随机字串
		String verifyCode = VerifyCodeUtil.generateVerifyCode(4);
		session.put("randomCode", verifyCode); // 写入session中
		// 生成图片
		int w = 100, h = 50;
		VerifyCodeUtil.outputImage(w, h, response.getOutputStream(),
				verifyCode, response);
		return null;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	@Override
	public void setServletResponse(HttpServletResponse response) {
		this.response = response;
	}

}
