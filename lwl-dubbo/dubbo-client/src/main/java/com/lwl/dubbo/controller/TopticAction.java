package com.lwl.dubbo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;

import com.alibaba.fastjson.JSON;
import com.opensymphony.xwork2.ActionSupport;
import com.lwl.dubbo.domain.Teacher;
import com.lwl.dubbo.domain.Toptic;
import com.lwl.dubbo.dto.Result;
import com.lwl.dubbo.service.IStudentService;
import com.lwl.dubbo.service.ITeacherService;
import com.lwl.dubbo.service.ITopticService;



/**
 * 课题 ACTION
 * 
 * @author
 * 
 */
public class TopticAction extends ActionSupport implements SessionAware {

	private static final long serialVersionUID = 1L;
	private String forwardPage = "";
	private Map<String, Object> session;
	@Resource
	private ITopticService topticService;
	@Resource
	private ITeacherService teacherService;
	@Resource
	private IStudentService studentService;
	
	private Toptic toptic;
	private int id;

	HttpServletRequest request = ServletActionContext.getRequest();
	HttpServletResponse response =ServletActionContext.getResponse();
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	/**
	 * 跳转到选择课题的页面
	 * 
	 * 
	 * @return
	 */
	public String toChoice() {
//		String username = session.get("username").toString(); 
//		String role = session.get("role").toString();					
//		Map<String, String> tchMap = studentService.forwardChoiceTopticPage(username, role);
//		session.put("tchMap", tchMap); // 向页面传递一个教师的集合,提供给下拉框选择		
	
        List<Teacher> list = studentService.getTeacherList();
		
		HttpServletRequest request = ServletActionContext.getRequest();
		
		HttpSession se = request.getSession();
		
		se.setAttribute("teacher", list);
			
		forwardPage = "/WEB-INF/page/student/choiceToptic.jsp";
		return SUCCESS;
	}

	/**
	 * 选择课题
	 * 
	 * @return
	 */
	public String choice() {
		String username = session.get("username").toString(); // 获取登陆使用的帐号
		String tchId = request.getParameter("tchId");
	//	System.out.println("老师是"+tchId);
		String topName = request.getParameter("topName");
	//	System.out.println("题目是"+topName);
		toptic.setTopName(topName);
		Teacher teacher = new Teacher();
		teacher.setTchId(tchId);
		toptic.setTeacher(teacher);
		forwardPage = studentService.choiceToptic(username, toptic,session);
		System.out.println("-------------------"+forwardPage);
		return "forwardTchMessage";
	}
	
	public void chiceCourse() throws IOException{	
		HttpSession se = request.getSession();	
		String tchId = request.getParameter("tchId");
		List<Toptic> list = topticService.list("from Toptic where ttid = '"
				+ tchId + "'", null);
		se.setAttribute("toptic", list);
		
		List<Result> toList =  new ArrayList<Result>();
		/* 设置字符集为'UTF-8' */
		response.setCharacterEncoding("UTF-8");
		Result res = null;
		for (int i = 0; i < list.size(); i++) {
			res = new Result();
			res.setId(list.get(i).getId()+"");//int转String 加一个空字符
			res.setTopName(list.get(i).getTopName());
			toList.add(res);
		}
		String json = JSON.toJSONString(toList);
		response.getWriter().print(json);		
	}
	 
	
	/**
	 * 课题列表
	 * 
	 * @return
	 */
	public String query() {
		String username = session.get("username").toString(); // 获取学生登陆使用的学号
		List<Toptic> list = topticService.list(
				"from Toptic where tsid = '" + username + "'", null);
		session.put("list", list);
		forwardPage = "/WEB-INF/page/student/topticMessage.jsp";
		return SUCCESS;
	}

	/**
	 * 跳转到更新选题的页面
	 * 
	 * @return
	 */
	public String toUpdateToptic() {
		String username = session.get("username").toString(); // 获取登陆使用的帐号
		List<Teacher> list = teacherService.list("from Teacher where tchId = '" + username.substring(0, 8) + "' ", null);
		Map<String, String> tchMap = new HashMap<String, String>(); // key:教师编号，value:教师名字

		for (Teacher teacher : list) {
			tchMap.put(teacher.getTchId(), teacher.getTchName());
		}
		session.put("tchMap", tchMap); // 当学生被拒绝时重新选择老师
		Toptic toptic = topticService.load(id);
		session.put("toptic", toptic);
		forwardPage = "/WEB-INF/page/student/stuUpdateToptic.jsp";
		return SUCCESS;
	}

	/**
	 * 跳转到发布选题
	 */
	public String toAdd(){		
		forwardPage = "/WEB-INF/page/teacher/teaaddToptic.jsp";
		return SUCCESS;		
	}
	
	/**
	 * 发布选题
	
	public String add(){
		String username = session.get("username").toString();
		// 获取角色：学生、老师、管理员
		String role = session.get("role").toString();
		if (StringUtils.isNotBlank(role) && "teacher".equals(role)) {
			String sql = "INSERT INTO toptic(topName,ttid) "
					+ "VALUES('"+toptic.getTopName()+"','"+username+"')";
			
			System.out.println(username);
			topticService.insertBySql(sql);		
		} else {
			System.out.println("只有老师能发布课题...");
		}
		forwardPage = "Teacher_queryToptic.action";
		return "forwardTchMessage";			
	}
	 */
	
	/**
	 * 更新选题
	 * 
	 * @return
	 */
	public String updateToptic() {
		String sql = "update toptic set topName='" + toptic.getTopName()
		+ "',status='" + toptic.getStatus() + "',ttid='"
		+ toptic.getTeacher().getTchId() + "' where id='"
		+ toptic.getId() + "'";
		topticService.insertBySql(sql);
		String role = session.get("role").toString();
		if (StringUtils.isNotBlank(role) && "student".equals(role))
			forwardPage = "Toptic_query.action";
		else if (StringUtils.isNotBlank(role) && "teacher".equals(role))
			forwardPage = "Teacher_queryToptic.action";
		return "forwardTchMessage";
	}

	public String getForwardPage() {
		return forwardPage;
	}

	public void setForwardPage(String forwardPage) {
		this.forwardPage = forwardPage;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public Toptic getToptic() {
		return toptic;
	}

	public void setToptic(Toptic toptic) {
		this.toptic = toptic;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
