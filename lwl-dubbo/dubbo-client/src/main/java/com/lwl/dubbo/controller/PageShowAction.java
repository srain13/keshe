package com.lwl.dubbo.controller;

import java.util.List;
import java.util.Map;  

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;

import com.opensymphony.xwork2.ActionContext;  
import com.lwl.dubbo.domain.PageShow;
import com.lwl.dubbo.service.IPageShowService;


@SuppressWarnings("serial")
public class PageShowAction extends BaseAction{ 
	 
	@Resource
	private IPageShowService pageShowService;

    private int pageNow = 1;// 动态改变 页面取得  
    private int pageSize = 4;// 固定不变  
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	public String getAllXs() {     	
    	String username = session.get("username").toString();
		// 获取角色：学生、老师、管理员
		String role = session.get("role").toString();
    	
		Map request = (Map) ActionContext.getContext().get("request");  
		if(StringUtils.isNotBlank(role)&&"admin".equals(role)){
		    List list = pageShowService.getAllXs("from Student",pageNow, pageSize);  
			session.put("list", list);
			 PageShow page = new PageShow(pageNow, pageShowService.findXsSize(), pageSize);// 实例化分页对象  
	         request.put("page", page);// 保存到request  
		}else if(StringUtils.isNotBlank(role)&&"teacher".equals(role)){
        List list = pageShowService.getAllXs("from Student",pageNow, pageSize);  
            session.put("list", list);// 保存在session             
            PageShow page = new PageShow(pageNow, pageShowService.findXsSize(), pageSize);// 实例化分页对象  
            request.put("page", page);// 保存到request  
        }else {
        	 List list = this.pageShowService.getAllXs("from Student where stuId = '"+username+"'",pageNow, pageSize); 	//学生查看自己的信息
			session.put("list", list);
		    PageShow page = new PageShow(pageNow, pageShowService.findXsSize(), pageSize);// 实例化分页对象  
            request.put("page", page);// 保存到request  
        }       
            setForwardPage("/WEB-INF/page/student/studentMessage.jsp");
            return SUCCESS;
    }

	public void setPageShowService(IPageShowService pageShowService) {
		this.pageShowService = pageShowService;
	}

	public void setPageNow(int pageNow) {
		this.pageNow = pageNow;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}  
    
    
}  