package com.lwl.dubbo.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import com.lwl.dubbo.domain.Cclass;
import com.lwl.dubbo.domain.Student;
import com.lwl.dubbo.service.IStudentService;
import com.lwl.dubbo.utils.RegUtil;



/**
 * 学生	ACTION
 * @author 
 *
 */
public class StudentAction extends BaseAction{

	private static final long serialVersionUID = 1L;
	@Resource	
	private IStudentService studentService;
	@Autowired
	private Student student;
	private Cclass cclass;
		
	/**
	 * 查询所有的学生
	 * @return
	
	public String queryStudent(){
		String username = session.get("username").toString();
		// 获取角色：学生、老师、管理员
		String role = session.get("role").toString();
		List<Student> list = null;
		if(StringUtils.isNotBlank(role)&&"admin".equals(role)){
			list = studentService.list("from Student", null);	//管理员查看所有的学生信息
		}else if(StringUtils.isNotBlank(role)&&"teacher".equals(role)){
			list =studentService.list("from Student", null);	//教师查看已选自己的学生信息		
		}else{
			list =studentService.list("from Student where stuId = '"+username+"'", null);	//学生查看自己的信息
		}
		session.put("list", list);
		setForwardPage("/WEB-INF/page/student/studentMessage.jsp");
		return SUCCESS;
	}
 */	
	
	
	/**
	 * 根据学号查询学生
	 * @return
	 */
	public String queryStudentByID(){
		String username = session.get("username").toString();
		// 获取角色：学生、老师、管理员
		String role = session.get("role").toString();
		List<Student> list = null;
		if(StringUtils.isNotBlank(role)&&"admin".equals(role)){
			list = studentService.list("from Student where stuId = '"+student.getStuId()+"'", null);	//管理员根据ID查询学生信息
		}else if(StringUtils.isNotBlank(role)&&"teacher".equals(role)){
			list = studentService.list("from Student where stuId = '"+student.getStuId()+"'", null);	//教师查看已选自己的学生信息			
		}else{
			list =studentService.list("from Student where stuId = '"+username+"'", null);
		}
		session.put("list", list);
		setForwardPage("/WEB-INF/page/student/studentMessage.jsp");
		return SUCCESS;
	}


	/**
	 * 根据学生姓名模糊查询学生
	 * @return
	 */
	public String queryStudentByName()throws Exception{
		String username = session.get("username").toString();
		HttpServletRequest request = ServletActionContext.getRequest();
		
		String stuName = request.getParameter("stuName");	
		// 获取角色：学生、老师、管理员
		String role = session.get("role").toString();
		List<Student> list = null;
		if(StringUtils.isNotBlank(role)&&"admin".equals(role)){
			list = studentService.list("from Student where stuName like '%"+stuName+"%'", null);	//管理员根据ID查询学生信息
		}else if(StringUtils.isNotBlank(role)&&"teacher".equals(role)){
			list = studentService.list("from Student where stuName like '%"+stuName+"%' ", null);	//教师查看已选自己的学生信息			
		}else{
			list =studentService.list("from Student where stuName = '"+username+"'", null);
		}
		session.put("list", list);
		setForwardPage("/WEB-INF/page/student/studentMessage.jsp");
		return SUCCESS;
	}
	
		
	/**
	 * 跳转到增加学生的页面
	 * 
	 * @return
	 */
	public String toAdd(){
		setForwardPage("/WEB-INF/page/student/addStudent.jsp");
		return SUCCESS;
	}
	
	public String add(){			
		// 获取角色：学生、老师、管理员
		String role = session.get("role").toString();
		if(StringUtils.isNotBlank(role)&&"teacher".equals(role)){
		//查询该班级学生的最大值
		@SuppressWarnings("rawtypes")
		List list = studentService.queryBySql("SELECT MAX(stuId) FROM student ");
		
		String stuId = RegUtil.getDigit(list.toString());

		if(stuId==null){
			stuId = cclass.getClassId()+student.getStuNo()+"001";
		}else{
			stuId  = String.valueOf(Long.valueOf(stuId)+1);
		}
				
		String sql = "INSERT INTO student(stuId,stuName,stuSex,stuNo,password) VALUES('"+stuId+"','"+student.getStuName()+"','"+student.getStuSex()+"','"+student.getStuNo()+"','123456')";
		studentService.insertBySql(sql);
		}
		setForwardPage("Student_queryStudent.action");
		return "forwardTchMessage";
	}
	
	/**
	 * 跳转到更新学生的页面
	 * 
	 * @return
	 */
	public String toUpdate(){		
		setForwardPage("/WEB-INF/page/student/updateStudent.jsp");
		return SUCCESS;
	}
	
	public String update(){	
		String sql = "UPDATE student SET stuName='"+student.getStuName()+"',stuSex='"+student.getStuSex()+"',password='"+student.getPassword()+"'"
				+ "  Where stuId='"+student.getStuId()+"' ";
		studentService.insertBySql(sql);
		setForwardPage("Student_queryStudent.action");
		return "forwardTchMessage";
	}
		
	
	/**
	 * 根据学号删除学生
	 * @return
	 */
	public String delete(){
		studentService.delete(student.getStuId());
		setForwardPage("Student_queryStudent.action");
		return "forwardTchMessage";
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public Cclass getCclass() {
		return cclass;
	}

	public void setCclass(Cclass cclass) {
		this.cclass = cclass;
	}
	

}
