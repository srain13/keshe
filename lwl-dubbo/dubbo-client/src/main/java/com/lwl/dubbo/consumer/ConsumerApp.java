package com.lwl.dubbo.consumer;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.lwl.dubbo.service.IBaseService;
import com.lwl.dubbo.service.ICclassService;
import com.lwl.dubbo.service.IPageShowService;
import com.lwl.dubbo.service.IStudentService;
import com.lwl.dubbo.service.ITeacherService;
import com.lwl.dubbo.service.ITopticService;
import com.lwl.dubbo.service.IUserService;


public class ConsumerApp {

    public static void main(String[] args) throws Exception {
        //读取配置文件
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"dubbo_config.xml"});
        //获取在zookeeper注册的服务接口
        context.start();
        
        IUserService userService = (IUserService) context.getBean("userService");
        IStudentService studentService = (IStudentService) context.getBean("studentService");
        ITeacherService teacherService = (ITeacherService) context.getBean("teacherService");       
        ITopticService topticService = (ITopticService) context.getBean("topticService");
        ICclassService cclassService = (ICclassService) context.getBean("cclassService");
        IPageShowService pageshowService = (IPageShowService) context.getBean("pageshowService");
       
        IBaseService basesService = (IBaseService) context.getBean("basesService");
        //不让控制台消失，按任意键结束
        System.in.read();
    }
}