package com.lwl.dubbo.controller;

import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

public abstract class BaseAction extends ActionSupport implements SessionAware{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1251914412452620767L;
	
	protected Map<String, Object> session;
	protected String forwardPage;
	
	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;		
	}

	public String getForwardPage() {
		return forwardPage;
	}

	public void setForwardPage(String forwardPage) {
		this.forwardPage = forwardPage;
	}

}
