package com.lwl.dubbo.controller;


import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.interceptor.SessionAware;


import com.opensymphony.xwork2.ActionSupport;
import com.lwl.dubbo.domain.Cclass;
import com.lwl.dubbo.domain.Teacher;
import com.lwl.dubbo.domain.Toptic;
import com.lwl.dubbo.service.ITeacherService;
import com.lwl.dubbo.service.ITopticService;
import com.lwl.dubbo.utils.RegUtil;

/**
 * 教师 ACTION
 * 
 * @author 
 * 
 */
public class TeacherAction extends ActionSupport implements SessionAware {
	private static final long serialVersionUID = 1L;
	@Resource
	private ITeacherService teacherService;
	
	@Resource
	private ITopticService topticService;
	private String forwardPage = "";
	private Map<String, Object> session;
	private Teacher teacher;
	private Cclass cclass;
	private int id; // 用于跳转到更新页面
	private String status;

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	/**
	 * 教师列表
	 * 
	 * @return
	 */
	public String queryTeacher() {
		String username = session.get("username").toString();
		// 获取角色：学生、老师、管理员
		String role = session.get("role").toString();
		List<Teacher> list = null;
		if (StringUtils.isNotBlank(role) && "admin".equals(role)) {
			list = teacherService.list("from Teacher", null);
		} else if (StringUtils.isNotBlank(role) && "teacher".equals(role)) {
			list = teacherService.list("from Teacher where tchId = '"
					+ username + "' ", null);
		} else {
			System.out.println("学生登陆...");
		}
		session.put("list", list);
		forwardPage = "/WEB-INF/page/teacher/teacherMessage.jsp";
		return SUCCESS;
	}

	/**
	 * 通过教师编号查询教师信息
	 * 
	 * @return
	 */
	public String queryTeacherByID() {
		String username = session.get("username").toString();
		// 获取角色：学生、老师、管理员
		String role = session.get("role").toString();
		List<Teacher> list = null;
		if (StringUtils.isNotBlank(role) && "admin".equals(role)) {
			list = teacherService.list("from Teacher where tchId = '"
					+ teacher.getTchId() + "' ", null);
		} else if (StringUtils.isNotBlank(role) && "teacher".equals(role)) {
			list = teacherService.list("from Teacher where tchId = '"
					+ username + "' ", null);
		} else {
			System.out.println("学生登陆...");
		}
		session.put("list", list);
		forwardPage = "/WEB-INF/page/teacher/teacherMessage.jsp";
		return SUCCESS;
	}

	/**
	 * 跳转到增加老师的页面
	 * 
	 * @return
	 */
	public String toAdd() {		
		forwardPage = "/WEB-INF/page/teacher/addTeacher.jsp";
		return SUCCESS;
	}	
	
	/**
	 * 增加教师
	 * 
	 * @return
	 */
	public String add() {
		// 查询教师编号的最大值
		@SuppressWarnings("rawtypes")
		List list = teacherService
				.queryBySql("SELECT MAX(tchId) FROM teacher");
		String tchId = RegUtil.getDigit(list.toString());
		if (null == tchId) {
			tchId =cclass.getClassId()  + "001";
		} else {
			tchId = String.valueOf(Long.valueOf(tchId) + 1);
		}
		String sql = "INSERT INTO teacher(tchId,tchName,tchSex,subject,password) "
				+ "VALUES('"+tchId+"','"+teacher.getTchName()+"'"
				+ ",'"+teacher.getTchSex()+"'"
				+ ",'"+teacher.getSubject()+"','123456')";
		teacherService.insertBySql(sql);
		forwardPage = "Teacher_queryTeacher.action";
		return "forwardTchMessage";
	}

	/**
	 * 跳转到更新老师的页面
	 * 
	 * @return
	 */
	public String toUpdate(){		
		setForwardPage("/WEB-INF/page/teacher/updateTeacher.jsp");
		return SUCCESS;
	}
	
	public String update(){	
		String sql = "UPDATE teacher SET tchName='"+teacher.getTchName()+"'"
				+ ",tchSex='"+teacher.getTchSex()+"'"
				+ ",subject='"+teacher.getSubject()+"'"
				+ ",password='"+teacher.getPassword()+"'"
				+ "  Where tchId='"+teacher.getTchId()+"' ";
		teacherService.insertBySql(sql);
		setForwardPage("Teacher_queryTeacher.action");
		return "forwardTchMessage";
	}
		
	
	/**
	 * 删除教师
	 * 
	 * @return
	 */
	public String delete() {
		teacherService.delete(teacher.getTchId());
		forwardPage = "Teacher_queryTeacher.action";
		return "forwardTchMessage";
	}
	
	
	/**
	 * 选题信息
	 * 
	 * @return
	 */
	public String queryToptic() {
		String username = session.get("username").toString();
		// 获取角色：学生、老师、管理员
		String role = session.get("role").toString();
		List<Toptic> list = null;
		if (StringUtils.isNotBlank(role) && "teacher".equals(role)){
			list = topticService.list("from Toptic where ttid = '"
					+ username + "'", null);
			forwardPage = "/WEB-INF/page/teacher/teacherTopticMessage.jsp";
		} else if (StringUtils.isNotBlank(role) && "admin".equals(role))  {
			list = topticService.list("from Toptic", null);
			forwardPage = "/WEB-INF/page/teacher/teacherTopticMessage.jsp";
		} else {
			forwardPage = "/WEB-INF/page/teacher/teacherTopticMessage.jsp";
		}
		session.put("list", list);
		return SUCCESS;
	}
	
	/**
	 * 通过状态查询选题列表
	 * 
	 * @return
	 */
	public String queryTopticByStatus() {
		String username = session.get("username").toString();
		List<Toptic> list = null;
		list = topticService.list("from Toptic where ttid = '"
				+ username + "' and status = '" + status + "'", null);
		session.put("list", list);
		forwardPage = "/WEB-INF/page/teacher/teacherTopticMessage.jsp";
		return SUCCESS;
	}

	/**
	 * 修改选题状态：确认、拒绝
	 * 
	 * @return
	 */
	public String topticStatus() {
		String role = session.get("role").toString();
		teacherService.modifyTopticStatue(id,  role, status);
		forwardPage = "Teacher_queryToptic.action";
		return "forwardTchMessage";
	}

	/**
	 * 跳转到更新选题的页面
	 * 
	 * @return
	 */
	public String toUpdateToptic() {
		Toptic toptic = topticService.load(id);
		session.put("toptic", toptic);
		forwardPage = "/WEB-INF/page/teacher/teaUpdateToptic.jsp";
		return SUCCESS;
	}

	public String getForwardPage() {
		return forwardPage;
	}

	public void setForwardPage(String forwardPage) {
		this.forwardPage = forwardPage;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Cclass getCclass() {
		return cclass;
	}

	public void setCclass(Cclass cclass) {
		this.cclass = cclass;
	}

}
