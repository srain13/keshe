package com.lwl.dubbo.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.lwl.dubbo.domain.Toptic;
import com.lwl.dubbo.service.ITopticService;

public class DownloadAction extends ActionSupport {  
//	private final static String UPLOADDIR = "/upload";   
    
	private static final long serialVersionUID = 4860648672293701316L;
	
	@Resource
	private ITopticService topticService;
	
	private InputStream fileInput;  
	
    private String fileName;  
    
    private String tchId;//教师ID
    
    private Integer topId;//课设ID
    
    public void setTchId(String tchId) {
		this.tchId = tchId;
	}

	public void setTopId(Integer topId) {
		this.topId = topId;
	}

	public String getFileName() {  
		return fileName;  
    }  
  
    public void setFileName(String fileName) {  
        this.fileName = fileName;  
    }  
  
    public InputStream getFileInput() {  
    	return fileInput;
    }  
  
    public void setFileInput(InputStream fileInput) {  
        this.fileInput = fileInput;  
    }  
      
    public void myExecute() throws Exception{  
    	
    	Toptic toptic = topticService.queryTopticInfo(tchId, topId);
    	fileName = toptic.getFileName();
    	String url = toptic.getSavePath()+"/"+toptic.getFileName();
    	url = new String(url.getBytes(),"UTF-8"); 
//      fileInput=ServletActionContext.getServletContext().getResourceAsStream(url);  
        File file=new File(url);
        FileInputStream fin = new FileInputStream(file);
        fileDownload(fin);
    }  
    
    public void fileDownload(FileInputStream in) {
    	BufferedInputStream is = null;
    	BufferedOutputStream os = null;
    	try {
			HttpServletResponse response = ServletActionContext.getResponse();
			response.setContentType("application/octet-stream;charset=UTF-8");
			// 设置弹出框提示的文件名
//			response.addHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode(fileName, "UTF-8"));
			//解决名称中文乱码
			response.setHeader("Content-disposition", "attachment; filename="+new String(fileName.getBytes("gbk"),"iso-8859-1"));
//    	第二步：解析输入流
			// 这里的in为你的输入流
			 is = new BufferedInputStream(in);
			// 准备缓冲区
			byte[] buffer = new byte[4096];
//    	第三步：将输入流转换为输出流
			 os = new BufferedOutputStream(response.getOutputStream());
			int offset = 0;
			while((offset = is.read(buffer, 0, 4096)) != -1) {
			os.write(buffer, 0, offset);
				
			}
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
//        	第四步：关闭输入输出流
    		if (os != null) {
    			try {
    				os.close();
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
    		if (is != null) {
    			try {
    				
    				is.close();
    			} catch (IOException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
		}
    }
    
    
}  